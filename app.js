import express from 'express';
import express_validator from 'express-validator';
import eoc from 'express-openid-connect';
import dotenv from 'dotenv';
import path from 'path';
import { fileURLToPath } from 'url';
import bodyParser from 'body-parser';
import pkg from 'pg';
import { getComments, postCommentUnsafe, createDB } from './db/db.js';

dotenv.config();
createDB();

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

const app = express();
const __filename = fileURLToPath(import.meta.url);
const { auth, requiresAuth } = eoc;
const { check } = express_validator;

const { Pool } = pkg;
const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB,
    password: process.env.DB_PASSWORD,
    port: 5432
});

const config = {
    authRequired: false,
    auth0Logout: true,
    secret: process.env.SECRET,
    baseURL: externalUrl || `https://localhost:${port}`,
    clientID: process.env.CLIENT_ID,
    issuerBaseURL: 'https://dev-d2x8nafabo40fem0.us.auth0.com',
    clientSecret: process.env.CLIENT_SECRET,
      authorizationParams: {
      response_type: 'code',
      //scope: "openid profile email"
      },
  };  
  
app.use(auth(config));

const __dirname = path.dirname(__filename);
app.set("views", path.join(__dirname, "src/views"));
app.set('view engine', 'ejs');

dotenv.config();
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', async (request, response, next) => {
    const user = request.oidc.isAuthenticated() ? request.oidc.user : undefined;
    const error = undefined;
    const comments = await getComments();
    response.render('index', {user, comments, error});
})

app.get('/searchComments', async (request, response, next) => {
    const user = request.oidc.isAuthenticated() ? request.oidc.user : undefined;
    const search = request.query.email;
    console.log(search);

    pool.query('SELECT * FROM comments_projekt2 WHERE email = \'' + search + '\'' ,(err, result) => {
        let comments = [];
        let error = undefined;
        if(result !== undefined)
            comments = result.rows;
        if(err)
            error = err;
        response.render('index', {user, comments, error})
    });
})

app.post('/searchComments', check('email').isEmail().normalizeEmail(),async (request, response, next) => {
    var user = request.oidc.isAuthenticated() ? request.oidc.user : undefined;
    var search = request.body.email;
    
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zAZ]{2,}))$/;        

    if(search == ""){
        const error = "Search field must be filled out!";
        const comments = await getComments();
        response.render('index', {user, comments, error});
        return;
    } else if (re.test(String(search).toLowerCase()) == false) {
        const error = "Invalid parameter!";
        const comments = await getComments();
        response.render('index', {user, comments, error});
        return;
    }

    if(search) {
        pool.query('SELECT * FROM comments_projekt2 WHERE email = $1', [search], (error, result) => {
            var comments = [];
            var err = undefined;
            if(result !== undefined)
                comments = result.rows;
            if(error){
                console.log(error);
                err = "Invalid parameter!";
            }
            response.render('index', {user, comments, error: err});
        })
    }
});

app.get('/safeFromCSRF', async (request, response, next) => {
    const user = request.oidc.isAuthenticated() ? request.oidc.user : undefined;
    const error = undefined;
    const comments = await getComments();
    response.render('safeFromCSRF', {user, comments, error});
})

app.get('/notSafeFromCSRF', async (request, response, next) => {
    const user = request.oidc.isAuthenticated() ? request.oidc.user : undefined;
    const error = undefined;
    const comments = await getComments();
    response.render('notSafeFromCSRF', {user, comments, error});
})

app.post('/commentUnsafe', requiresAuth(), async (request, response, next) => {
    const email = request.oidc.user.email;
    const content = request.body.comment;

    await postCommentUnsafe(email, content);

    response.redirect('/');

})

if (externalUrl) {
    const hostname = '127.0.0.1';
    app.listen(port, hostname, () => {
    console.log(`Server locally running at http://${hostname}:${port}/ and from
    outside on ${externalUrl}`);
    });
  }