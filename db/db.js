import pkg from 'pg';
import dotenv from 'dotenv';

dotenv.config();

const { Pool } = pkg;

const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB,
    password: process.env.DB_PASSWORD,
    port: 5432
});

export async function createDB() {
    const sql = `CREATE TABLE public.comments_projekt2
    (
        id integer NOT NULL DEFAULT nextval('comments_id_seq'::regclass),
        email character varying COLLATE pg_catalog."default" NOT NULL,
        content character varying COLLATE pg_catalog."default" NOT NULL,
        CONSTRAINT comments_pkey PRIMARY KEY (id)
    )
    
    TABLESPACE pg_default;
    
    ALTER TABLE public.comments_projekt2
        OWNER to matej;`;
    pool.query(sql);
}

export async function getComments() {
    const results = await pool.query('SELECT * FROM comments_projekt2');
    return results.rows;
}

export async function postCommentUnsafe(email, content) {
    pool.query(`INSERT INTO comments_projekt2(email, content) VALUES ('` + email + `', '` + content + `')`, (err, rows) => {
        if(err) throw err;
        return rows;
    });
}